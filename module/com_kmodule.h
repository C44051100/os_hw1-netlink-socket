#ifndef COM_KMODULE_H
#define COM_KMODULE_H

#include <linux/string.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/pid.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <linux/timer.h>
#include <linux/time.h>

#define NETLINK_USER 31

struct mailbox
{
    //0: unqueued
    //1: queued
    unsigned char type;
    unsigned char msg_data_count;
    struct msg_data *msg_data_head;
    struct msg_data *msg_data_tail;
};

struct msg_data
{
    char buf[256];
    struct msg_data *next;
};

struct sock *nl_sk = NULL;

int register_table[1000]= {0};
int iid;
char id[10];
char type[10];
char act[15];
char data[256];

struct mailbox box[1000];

//struct mailbox box[1000];

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Apple pie");
MODULE_DESCRIPTION("A Simple Hello World module");

#endif  //ifndef COM_KMODULE_H
