#include "com_kmodule.h"

static void hello_nl_recv_msg(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    struct sk_buff *skb_out;
    int msg_size;
    char *re_msg;
    char *s_msg;
    char *r_msg;
    int res;
    int count=0;
    int num=0;
    int i=0;
    iid=0;

    memset(id,'\0',sizeof(id));
    memset(type,'\0',sizeof(type));
    memset(act,'\0',sizeof(act));
    memset(data,'\0',sizeof(data));

    printk(KERN_INFO "Entering: %s\n", __FUNCTION__);

    //msg_size=strlen(msg);

    nlh=(struct nlmsghdr*)skb->data;
    printk(KERN_INFO "Netlink received msg payload:%s\n",(char*)nlmsg_data(nlh));
    pid = nlh->nlmsg_pid; /*pid of sending process */

    re_msg = vmalloc( strlen(NLMSG_DATA(nlh)) * sizeof(char) + 1 );
    memset(re_msg,'\0',strlen(NLMSG_DATA(nlh)) * sizeof(char) + 1);
    memcpy(re_msg, NLMSG_DATA(nlh), strlen(NLMSG_DATA(nlh)) );

    s_msg = vmalloc( strlen(NLMSG_DATA(nlh)) * sizeof(char) + 1 );
    memset(s_msg,'\0',strlen(NLMSG_DATA(nlh)) * sizeof(char) + 1);
    memcpy(s_msg, NLMSG_DATA(nlh), strlen(NLMSG_DATA(nlh)) );

    r_msg = vmalloc( strlen(NLMSG_DATA(nlh)) * sizeof(char) + 1 );
    memset(r_msg,'\0',strlen(NLMSG_DATA(nlh)) * sizeof(char) + 1);
    memcpy(r_msg, NLMSG_DATA(nlh), strlen(NLMSG_DATA(nlh)) );
    //printk(KERN_INFO "Netlink received msg payload: %s\n",re_msg);

    for(i=0; i<4; i++)
    {
        act[i]+=re_msg[i];
    }

    //register
    if(strcmp(act,"Regi")==0)
    {
        for(i=0; i<strlen(re_msg); i++)
        {

            if(re_msg[i]=='='||re_msg[i]==',')
            {
                num=0;
                count++;
                continue;
            }
            if(count==1)
            {
                id[num]+=re_msg[i];
                num++;
            }
            if(count==3)
            {
                type[num]+=re_msg[i];
                num++;
            }
        }

        kstrtoint(id,0,&iid);
        printk("Message from:     %d\n", iid);

        msg_size=strlen(re_msg);
        skb_out = nlmsg_new(msg_size,0);

        if(!skb_out)
        {
            printk(KERN_ERR "Failed to allocate new skb\n");
            return;
        }
        nlh=nlmsg_put(skb_out,0,0,NLMSG_DONE,msg_size,0);
        NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */


        //id has not been registered
        if(register_table[iid-1]==0)
        {
            strncpy(nlmsg_data(nlh),"Success",strlen("Success")+1);
            res=nlmsg_unicast(nl_sk,skb_out,pid);
            register_table[iid-1]=1;

            if(strcmp(type,"unqueued")==0)
            {
                box[iid-1].type=0;
            }
            if(strcmp(type,"queued")==0)
            {
                box[iid-1].type=1;
            }

            box[iid-1].msg_data_count=0;
            box[iid-1].msg_data_head=vmalloc(sizeof(struct msg_data));
            box[iid-1].msg_data_tail=vmalloc(sizeof(struct msg_data));
            box[iid-1].msg_data_head->next=vmalloc(sizeof(struct msg_data));
            box[iid-1].msg_data_head->next->next=box[iid-1].msg_data_tail;

            memset(box[iid-1].msg_data_head->buf,'\0',sizeof(box[iid-1].msg_data_head->buf));
            memset(box[iid-1].msg_data_tail->buf,'\0',sizeof(box[iid-1].msg_data_tail->buf));
            memset(box[iid-1].msg_data_head->next->buf,'\0',sizeof(box[iid-1].msg_data_head->next->buf));
        }
        //id has been registered
        else
        {
            strncpy(nlmsg_data(nlh),"Fail",strlen("Fail")+1);
            printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
            res=nlmsg_unicast(nl_sk,skb_out,pid);
        }
    }

    //send message
    if(strcmp(act,"Send")==0)
    {
        memset(data,'\0',sizeof(data));
        for(i=0; i<strlen(s_msg); i++)
        {

            if(s_msg[i]==' ')
            {
                
                if(count<2){
                    num=0;
                    count++;
                    continue;
                }
            }
            if(count==1)
            {
                id[num]+=s_msg[i]; //get destination id
                num++;
            }
            if(count==2)
            {
                data[num]+=s_msg[i];//get message
                printk("Data:     %c\n", data[num]);
                printk("Data:     %c\n", s_msg[i]);
                num++;
            }
        }

        kstrtoint(id,0,&iid);
        printk("Message to:     %d\n", iid);
        
        

        msg_size=strlen(s_msg);
        skb_out = nlmsg_new(msg_size,0);

        if(!skb_out)
        {
            printk(KERN_ERR "Failed to allocate new skb\n");
            return;
        }
        nlh=nlmsg_put(skb_out,0,0,NLMSG_DONE,msg_size,0);
        NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */

        //id has not ben registered
        if(register_table[iid-1]==0)
        {
            strncpy(nlmsg_data(nlh),"Unregistered destination",strlen("Unregistered destination")+1);
            res=nlmsg_unicast(nl_sk,skb_out,pid);
        }
        //id has been registered
        else
        {

            //if destination mailbox is unqueued
            if(box[iid-1].type==0)
            {
                strcpy(box[iid-1].msg_data_head->buf,data);
                strncpy(nlmsg_data(nlh),"Success",strlen("Success")+1);
                printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                res=nlmsg_unicast(nl_sk,skb_out,pid);
            }
            //if destination mailbox is queued
            if(box[iid-1].type==1)
            {
                if(box[iid-1].msg_data_count==3)
                {
                    strncpy(nlmsg_data(nlh),"Fail",strlen("Fail")+1);
                    printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                    res=nlmsg_unicast(nl_sk,skb_out,pid);
                }
                else
                {
                    if(box[iid-1].msg_data_count==0)
                    {
                        strcpy(box[iid-1].msg_data_head->buf,data);
                    }
                    if(box[iid-1].msg_data_count==1)
                    {
                        strcpy(box[iid-1].msg_data_head->next->buf,box[iid-1].msg_data_head->buf);
                        strcpy(box[iid-1].msg_data_head->buf,data);
                    }
                    if(box[iid-1].msg_data_count==2)
                    {
                        strcpy(box[iid-1].msg_data_head->next->next->buf,box[iid-1].msg_data_head->next->buf);
                        strcpy(box[iid-1].msg_data_head->next->buf,box[iid-1].msg_data_head->buf);
                        strcpy(box[iid-1].msg_data_head->buf,data);
                    }
                    strncpy(nlmsg_data(nlh),"Success",strlen("Success")+1);
                    printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                    res=nlmsg_unicast(nl_sk,skb_out,pid);
                    box[iid-1].msg_data_count++;
                }

            }

        }
    }
    //receive message
    if(strcmp(act,"Recv")==0)
    {
        for(i=0; i<strlen(r_msg); i++)
        {

            if(r_msg[i]==' ')
            {
                num=0;
                count++;
                continue;
            }
            if(count==1)
            {
                id[num]+=r_msg[i]; //get destination id
                num++;
            }
        }

        kstrtoint(id,0,&iid);

        printk("Message from:     %d\n", iid);

        msg_size=strlen(r_msg);
        skb_out = nlmsg_new(msg_size,0);
        if(box[iid-1].type==0)
        {
            msg_size=strlen(box[iid-1].msg_data_head->buf)+1;
            skb_out = nlmsg_new(msg_size,0);
        }
        if(box[iid-1].type==1)
        {
            if(box[iid-1].msg_data_count==1)
            {
                msg_size=strlen(box[iid-1].msg_data_head->buf)+1;
                skb_out = nlmsg_new(msg_size,0);
            }
            if(box[iid-1].msg_data_count==2)
            {
                msg_size=strlen(box[iid-1].msg_data_head->next->buf)+1;
                skb_out = nlmsg_new(msg_size,0);
            }
            if(box[iid-1].msg_data_count==3)
            {
                msg_size=strlen(box[iid-1].msg_data_head->next->next->buf)+1;
                skb_out = nlmsg_new(msg_size,0);
            }
        }


        if(!skb_out)
        {
            printk(KERN_ERR "Failed to allocate new skb\n");
            return;
        }
        nlh=nlmsg_put(skb_out,0,0,NLMSG_DONE,msg_size,0);
        NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */

        //id has not been registered
        if(register_table[iid-1]==0)
        {
            strncpy(nlmsg_data(nlh),"Fail",strlen("Fail")+1);
            res=nlmsg_unicast(nl_sk,skb_out,pid);

        }
        //id has been registered
        else
        {
            if(box[iid-1].type==0)
            {
                strncpy(nlmsg_data(nlh),box[iid-1].msg_data_head->buf,strlen(box[iid-1].msg_data_head->buf)+1);
                printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                res=nlmsg_unicast(nl_sk,skb_out,pid);
            }
            if(box[iid-1].type==1)
            {
                if(box[iid-1].msg_data_count==0)
                {
                    strncpy(nlmsg_data(nlh),"Fail",strlen("Fail")+1);
                    printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                    res=nlmsg_unicast(nl_sk,skb_out,pid);
                }
                else
                {
                    if(box[iid-1].msg_data_count==1)
                    {
                        strncpy(nlmsg_data(nlh),box[iid-1].msg_data_head->buf,strlen(box[iid-1].msg_data_head->buf)+1);
                        printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                        res=nlmsg_unicast(nl_sk,skb_out,pid);
                        memset(box[iid-1].msg_data_head->buf,'\0',sizeof(box[iid-1].msg_data_head->buf));
                    }
                    if(box[iid-1].msg_data_count==2)
                    {
                        strncpy(nlmsg_data(nlh),box[iid-1].msg_data_head->next->buf,strlen(box[iid-1].msg_data_head->next->buf)+1);
                        printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                        res=nlmsg_unicast(nl_sk,skb_out,pid);
                        memset(box[iid-1].msg_data_head->next->buf,'\0',sizeof(box[iid-1].msg_data_head->next->buf));
                    }
                    if(box[iid-1].msg_data_count==3)
                    {
                        strncpy(nlmsg_data(nlh),box[iid-1].msg_data_head->next->next->buf,strlen(box[iid-1].msg_data_head->next->next->buf)+1);
                        printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
                        res=nlmsg_unicast(nl_sk,skb_out,pid);
                        memset(box[iid-1].msg_data_head->next->next->buf,'\0',sizeof(box[iid-1].msg_data_head->next->next->buf));
                    }
                    box[iid-1].msg_data_count--;
                }
            }
        }
    }

    printk(KERN_INFO "Netlink received msg payload:%s\n",act);

    /*for(i=0; i<strlen(re_msg); i++)
    {

        if(re_msg[i]=='='||re_msg[i]==',')
        {
            num=0;
            count++;
            continue;
        }
        if(count==1)
        {
            id[num]+=re_msg[i];
            num++;
        }
        if(count==3)
        {
            type[num]+=re_msg[i];
            num++;
        }
    }

    kstrtoint(id,0,&iid);
    printk("Message from:     %d\n", iid);

    skb_out = nlmsg_new(msg_size,0);

    if(!skb_out)
    {
        printk(KERN_ERR "Failed to allocate new skb\n");
        return;
    }
    nlh=nlmsg_put(skb_out,0,0,NLMSG_DONE,msg_size,0);
    NETLINK_CB(skb_out).dst_group = 0;

    if(register_table[iid-1]==0)
    {
        strncpy(nlmsg_data(nlh),"Success",strlen("Success")+1);
        res=nlmsg_unicast(nl_sk,skb_out,pid);
        register_table[iid-1]=1;
    }
    else
    {
        strncpy(nlmsg_data(nlh),"Fail",strlen("Fail")+1);
        printk("Send message '%s'.\n",(char *)NLMSG_DATA(nlh));
        res=nlmsg_unicast(nl_sk,skb_out,pid);
    }*/

    //printk(KERN_INFO "Netlink send msg payload:%s\n",(char*)nlmsg_data(nlh));



    /*if(res<0)
        printk(KERN_INFO "Error while sending bak to user\n");*/
}

static int __init hello_init(void)
{
    struct netlink_kernel_cfg cfg =
    {
        .input = hello_nl_recv_msg
    };

    nl_sk = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);

    printk("Entering: %s\n",__FUNCTION__);

    if(!nl_sk)
    {
        printk(KERN_ALERT "Error creating socket.\n");
        return -10;
    }
    return 0;
}

static void __exit hello_exit(void)
{
    printk(KERN_INFO "exiting hello module\n");
    netlink_kernel_release(nl_sk);
}

module_init(hello_init);
module_exit(hello_exit);
