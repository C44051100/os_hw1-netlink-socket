#include "com_app.h"

void err_msg(const char* msg)
{
    printf("%s",msg);
}

int main(int argc, char *argv[])
{
    strcpy(id,argv[1]);

    strcpy(type,argv[2]);

    sock_fd=socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if(sock_fd<0)
        return -1;

    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid(); /* self pid */

    bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr));

    memset(&dest_addr, 0, sizeof(dest_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0; /* For Linux Kernel */
    dest_addr.nl_groups = 0; /* unicast */

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;

    strcpy(NLMSG_DATA(nlh), "Registration.");
    strcat(NLMSG_DATA(nlh), " ");
    strcat(NLMSG_DATA(nlh), "id=");
    strcat(NLMSG_DATA(nlh), id);
    strcat(NLMSG_DATA(nlh), ", ");
    strcat(NLMSG_DATA(nlh), "type=");
    strcat(NLMSG_DATA(nlh), type);

    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    nlh_in = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    iov_in.iov_base = (void *)nlh_in;
    iov_in.iov_len = nlh->nlmsg_len;
    msg_in.msg_iov = &iov_in;
    msg_in.msg_iovlen = 1;

    nlh_data = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    iov_data.iov_base = (void *)nlh_data;
    iov_data.iov_len = nlh->nlmsg_len;
    msg_data.msg_iov = &iov_data;
    msg_data.msg_iovlen = 1;

    sleep(2);
    //Sending message to kernel
    sendmsg(sock_fd,&msg,0);

    recvmsg(sock_fd, &msg_in, 0);
    printf("%s\n", (char *)NLMSG_DATA(nlh_in));


    if(strcmp(NLMSG_DATA(nlh_in),"Success")==0)
    {

        while(1)
        {
            scanf("%5s",act);

            if(strcmp(act,"Send")==0)
            {
                scanf("%5s",id_dest);
                fgets(space,1,stdin);
                //scanf("%255s",data);
                fgets(data,255,stdin);

                /*if(strlen(data)>=255 && data[255]!='\0')
                {
                    scanf("%1s",&no_use);
                    while(no_use!='\0')
                    {
                        scanf("%1s",&no_use);
                    }
                }*/

                strcpy(NLMSG_DATA(nlh), act);
                strcat(NLMSG_DATA(nlh)," ");
                strcat(NLMSG_DATA(nlh),id_dest);
                strcat(NLMSG_DATA(nlh)," ");
                strcat(NLMSG_DATA(nlh),data);
            }
            if(strcmp(act,"Recv")==0)
            {
                strcpy(NLMSG_DATA(nlh), act);
                strcat(NLMSG_DATA(nlh)," ");
                strcat(NLMSG_DATA(nlh),id);
            }

            sleep(2);
            //Sending message to kernel
            sendmsg(sock_fd,&msg,0);

            recvmsg(sock_fd, &msg_in, 0);
            printf("%s\n", (char *)NLMSG_DATA(nlh_in));
        }

    }

    else
    {
        close(sock_fd);
        return 0;
    }

    close(sock_fd);

}
