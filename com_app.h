#ifndef COM_APP_H
#define COM_APP_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <asm/types.h>
#include <sys/socket.h>

#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#define NETLINK_USER 31
#define MAX_PAYLOAD 10240 /* maximum payload size*/

struct sockaddr_nl src_addr, dest_addr;
struct nlmsghdr *nlh = NULL;
struct iovec iov;
struct msghdr msg;
struct nlmsghdr *nlh_in = NULL;
struct iovec iov_in;
struct msghdr msg_in;
struct nlmsghdr *nlh_data = NULL;
struct iovec iov_data;
struct msghdr msg_data;
int sock_fd;

char id[10];
char type[10];
char act[10];
char id_dest[10];
char data[256];

int i;
char no_use;
char space[5];

char message_data[1000];


#endif  //ifndef COM_APP_H
